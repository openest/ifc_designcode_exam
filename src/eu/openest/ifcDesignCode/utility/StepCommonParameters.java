package eu.openest.ifcDesignCode.utility;

import java.io.IOException;

import com.steptools.schemas.ifc4.Population;
import com.steptools.stdev.Model;
import com.steptools.stdev.STDevException;
import com.steptools.stdev.p21.Part21Parser;

public class StepCommonParameters
{
	private Model mod;
	private Population pop;
	private String inputIfcFileName;
	
	public StepCommonParameters(String inputIfcFileName) throws STDevException, IOException
	{
		this.inputIfcFileName = inputIfcFileName;
		//Model mod;
		Part21Parser parser = new Part21Parser();
		this.mod = parser.parse(inputIfcFileName);
		this.pop = (Population)mod.getPopulation();
	}
	public Model getModel()
	{
		return this.mod;
	}
	public Population getPop()
	{
		return this.pop;
	}
}
