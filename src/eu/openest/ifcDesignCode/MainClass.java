package eu.openest.ifcDesignCode;

import java.io.File;
import java.io.IOException;
import java.util.*;

import org.w3c.dom.*;
import javax.xml.parsers.*;
import javax.xml.xpath.*;

import com.steptools.schemas.ifc4.*;
import com.steptools.stdev.*;
import com.steptools.stdev.p21.EntityIDTable;
import eu.openest.ifcDesignCode.utility.StepCommonParameters;

public class MainClass
{
    public static void main(String[] args) throws Exception
    {
        System.out.println("========================= Start.. =========================");

        // initial input data
        String ifcFileName = "columnExample.ifc";
        String codeFileName = "DesignCodes_example.xml";
        String dataInIFC_inp = "Density";
        String objInCodes_inp = "T_Shape";
        //String objInCodes_inp = "V_Shape";
        String idInCodes_inp = "D001";

        String dataInIFC = "";
        String dataInCodes = "";

        // get value From IFC model
        dataInIFC = getValueFromIFC(ifcFileName, dataInIFC_inp, dataInIFC);

        // get value From design codes
        dataInCodes = getValueFromDesignCodes(codeFileName, objInCodes_inp, idInCodes_inp);

        System.out.println("Required " + dataInIFC_inp + " value for the Pier component:  " + dataInCodes);

        // Compare
        double dInIFC = Double.parseDouble(dataInIFC);
        double dInCodes = Double.parseDouble(dataInCodes);
        if (dInIFC > dInCodes)
        {
            System.out.println("***** Well designed. *****");
        }
        else
        {
            System.out.println("***** Required re-design. *****");
        }

        System.out.println("========================= Done! =========================");

    }

    private static String getValueFromDesignCodes(String codeFileName, String objInCodes_inp, String idInCodes_inp) throws Exception
    {
        String dataInCodes;
        DocumentBuilderFactory xmlDocFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder xmlBuilder = xmlDocFactory.newDocumentBuilder();
        Document xDoc = xmlBuilder.parse(new File(codeFileName));
        xDoc.getDocumentElement().normalize();

        dataInCodes = getCode(xDoc, objInCodes_inp, idInCodes_inp);
        return dataInCodes;
    }

    private static String getValueFromIFC(String ifcFileName, String dataInIFC_inp, String dataInIFC) throws STDevException, IOException
    {
        StepCommonParameters commonStep = new StepCommonParameters(ifcFileName);
        EntityIDTable entityIdTable = EntityIDTable.forModel(commonStep.getModel(), true);

        Population pop = commonStep.getPop();

        EntityExtent prods = pop.getExtent(Ifcbuildingelement.DOMAIN);
        EntityInstanceSetIterator itor = prods.extentIterator();
        while (itor.hasNext())
        {
            Ifcbuildingelement ifcBuildingElement_ = (Ifcbuildingelement) itor.next();
            if (ifcBuildingElement_.isa(Ifccolumn.DOMAIN))
            {
                Ifccolumn ifcColumn_ = (Ifccolumn) ifcBuildingElement_;
                EntityInstanceSet eis = ifcColumn_.usedin(pop, Ifcreldefinesbyproperties.relatedobjects_ATT);
                List<String> psetDataList = getPsetData(entityIdTable, eis, dataInIFC_inp);

                if (psetDataList.size() != 0)
                {
                    dataInIFC = psetDataList.get(2);
                    System.out.println(dataInIFC_inp + " value used in IFC model: " + dataInIFC);
                    break;
                }
                else
                {
                    System.out.println("null");
                }
            }
        }
        return dataInIFC;
    }

    private static String getCode(Document xDoc, String objName, String codeName) throws Exception
    {
        String rValue = "";
        XPath xpath = XPathFactory.newInstance().newXPath();

        String objExpr = "//@name[. = '" + objName + "']/..";
        List<Node> nodeList = evaluateXPath(xpath, xDoc, objExpr);
        if (nodeList.size() > 1)
        {
            System.out.println("Something went wrong.");
        }

        for (Node value : nodeList)
        {
            String designCodeExpr = ".//@codeName[. = '" + codeName + "']/..";
            List<Node> codeList = evaluateXPath(xpath, value, designCodeExpr);

            for (Node dCode : codeList)
            {
                rValue =  dCode.getTextContent();
            }
        }
        return rValue;
    }

    private static List<Node> evaluateXPath(XPath xpath, Object xNode, String xpathExpression) throws Exception
    {
        List<Node> values = new ArrayList<>();
        try
        {
            XPathExpression expr = xpath.compile(xpathExpression);
            NodeList nodes = (NodeList) expr.evaluate(xNode, XPathConstants.NODESET);

            for (int i = 0; i < nodes.getLength(); i++)
            {
                values.add(nodes.item(i));
            }

        } catch (XPathExpressionException e)
        {
            e.printStackTrace();
        }
        return values;
    }


    private static List<String> getPsetData(EntityIDTable entityIdTable, EntityInstanceSet eis, String queryString)
    {
        List<String> rVal = new ArrayList<String>();
        if (eis != null)
        {
            Iterator ifcR = eis.iterator();
            while(ifcR.hasNext())
            {
                Ifcreldefinesbyproperties ifcRelDefinesByProperties_ = (Ifcreldefinesbyproperties)ifcR.next();
                Ifcpropertysetdefinitionselect ifcPropertySetDefinitionSelect_ = ifcRelDefinesByProperties_.getRelatingpropertydefinition();
                if (ifcPropertySetDefinitionSelect_.isIfcpropertysetdefinition())
                {
                    Ifcpropertysetdefinition ifcPropertySetDefinition_ = ifcPropertySetDefinitionSelect_.getIfcpropertysetdefinition();
                    if (ifcPropertySetDefinition_.isa(Ifcpropertyset.DOMAIN))
                    {
                        Ifcpropertyset ifcPropertySet_ = (Ifcpropertyset) ifcPropertySetDefinition_;
                        SetIfcproperty setIfcProperty = ifcPropertySet_.getHasproperties();
                        if (setIfcProperty != null)
                        {
                            Iterator ifcPropertyItor = setIfcProperty.iterator();
                            while (ifcPropertyItor.hasNext())
                            {
                                Ifcproperty ifcProperty = (Ifcproperty) ifcPropertyItor.next();
                                if (ifcProperty.isa(Ifcpropertysinglevalue.DOMAIN))
                                {
                                    String queriedName = ((Ifcpropertysinglevalue)ifcProperty).getName();
                                    String queriedValue = "";

                                    if (queriedName.equals(queryString))
                                    {
                                        Ifcvalue val_ = ((Ifcpropertysinglevalue)ifcProperty).getNominalvalue();
                                        if (val_.isIfclabel())
                                        {
                                            queriedValue = val_.getIfclabel();
                                            String entityId = entityIdTable.getId(((Ifcpropertysinglevalue)ifcProperty)).toString();
                                            rVal.add(entityId);
                                            rVal.add(queriedName);
                                            rVal.add(queriedValue);
                                            return rVal;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return rVal;
    }
}
